(ns breakout.core
  (:require [quil.core :as q]
            [breakout.elements :as elements]
            [breakout.control :as control]
            [breakout.graphics :as graphics]
            [breakout.collision :as collision]))

(defn create-game []
  (elements/playfield
    (for [x (range 0 10)
          y (range 0 5)]
      (elements/brick x y))
    (elements/ball)
    (elements/board)))

(def game-state (atom (create-game)))

(defn update [game]
  (->> game
       (map #(control/time % (q/frame-count)))
       (collision/collision-check)))

(defn draw []
  (q/image (graphics/image :background) 0 0)
  (swap! game-state update)
  (doseq [element @game-state]
    (graphics/draw element)))

(defn setup []
  (graphics/load-images)
  (q/smooth)
  (q/no-cursor)
  (q/frame-rate 60))

(defn mouse-moved []
  (let [publisher (partial map #(control/mouse % {:x (q/mouse-x) :y (q/mouse-y)}))]
    (swap! game-state publisher)))

(defn mouse-pressed []
  (let [publisher (partial map control/start)]
    (reset! game-state (-> (create-game)
                           (publisher)))))

(q/defsketch breakout
  :title "Breakout"
  :setup setup
  :draw draw
  :size [320 200]
  :mouse-moved mouse-moved
  :mouse-pressed mouse-pressed)
