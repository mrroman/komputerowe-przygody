(ns breakout.graphics
  (:require [quil.core :as q]
            [breakout.elements :as elements]))

(def images (atom {}))

(defn load-images []
    (reset! images {:brick (q/load-image "brick.png")
                    :background (q/load-image "background.png")
                    :board (q/load-image "platform.png")
                    :ball (q/load-image "ball.png")}))

(defn image [id]
    (id @images))
    
(defprotocol GraphicElement
  (draw [this]))

(extend-type Object
  GraphicElement
  (draw [this] this))

(extend-type breakout.elements.Board
  GraphicElement
  (draw [this]
    (q/image (image :board) (:x this) 180))) 

(extend-type breakout.elements.Brick
  GraphicElement
  (draw [{:keys [x y w h]}]
    (q/image (image :brick) x y)))

(extend-type breakout.elements.Ball
  GraphicElement
  (draw [{:keys [x y]}]
    (q/image (image :ball) (- x 4) (- y 4))))

