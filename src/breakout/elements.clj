(ns breakout.elements)

(defrecord Board [x y w h])

(defn board []
  (->Board 120 180 60 6))

(defrecord Brick [x y w h])

(defn brick [col row]
  (->Brick (* col 32) (* row 10) 32 10))

(defrecord Ball [x y speed delta-x delta-y])

(defn ball 
  ([] (ball 2))
  ([speed] 
   (->Ball 160 100 speed 0 0)))

(defn ball? [ball]
  (instance? Ball ball))

(defn bounce-ball [{:keys [speed] :as ball} direction]
  (case direction
    :left (assoc ball :delta-x speed)
    :right (assoc ball :delta-x (- speed))
    :top (assoc ball :delta-y speed)
    :bottom (assoc ball :delta-y (- speed))
    ball))

(defn update-ball [{:keys [x y delta-x delta-y] :as ball}]
  (-> ball
    (assoc :x (+ x delta-x))
    (assoc :y (+ y delta-y))))

(defrecord Playfield [x1 y1 x2 y2])

(defn playfield [& elements]
  (cons (->Playfield 0 0 320 200) (flatten elements)))

