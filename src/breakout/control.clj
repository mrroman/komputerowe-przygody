(ns breakout.control
  (:require [breakout.elements :as elements]))

(defprotocol Control
  (start [this])
  (mouse [this position])
  (time [this frame]))

(extend-type Object
  Control
  (start [this] this)
  (mouse [this position] this)
  (time [this frame] this))

(extend-type breakout.elements.Ball
  Control
  (start [{speed :speed :as this}] (-> this
                                       (assoc :delta-x (- speed))
                                       (assoc :delta-y (- speed))))
  (mouse [this position] this)
  (time [this frame] (elements/update-ball this)))

(extend-type breakout.elements.Board
  Control
  (start [this] this)
  (mouse [this position] (assoc this :x (min 260 (:x position))))
  (time [this frame] this))

