(ns breakout.collision
  (:require [breakout.elements :as elements]))

(defn- only [k c]
  (->> c
       (map k)
       (filter identity)
       (flatten)
       (set)))

(defn- inside [a b]
  (let [{a-x :x a-y :y w :w h :h} a
        {b-x :x b-y :y} b]
    (and (< a-x b-x (+ a-x w))
         (< a-y b-y (+ a-y h)))))

(defmulti collide (fn [x y] [(class x) (class y)]))

(defmethod collide :default
  [x y]
  { :set [x y] })

(defmethod collide
  [breakout.elements.Ball breakout.elements.Playfield]
  [{x :x y :y :as ball} playfield]
  (let [new-ball (-> ball
                     (elements/bounce-ball (cond (<= x (:x1 playfield)) :left
                                                 (>= x (:x2 playfield)) :right))
                     (elements/bounce-ball (cond (<= y (:y1 playfield)) :top)))]
    (if (= ball new-ball)
      {:set [playfield ball]}
      {:replaced [ball] :set [playfield new-ball]})))

(defmethod collide
  [breakout.elements.Ball breakout.elements.Board]
  [ball board]
  (if (inside board ball)
    {:replaced [ball] :set [(elements/bounce-ball ball :bottom) board]}
    {:set [ball board]}))

(defmethod collide
  [breakout.elements.Ball breakout.elements.Brick]
  [ball brick]
  (if (inside brick ball)
    (let [new-ball (elements/bounce-ball ball :top)]
      (if (= new-ball ball)
        {:set [ball]}
        {:set [new-ball] :replaced [ball]}))
    {:set [brick ball]}))

(defn collision-check [game]
  (let [{balls true others false} (group-by elements/ball? game)
        output (for [ball balls
                     other others]
                 (collide ball other))
        all-set (only :set output)
        replaced (only :replaced output)]
    (remove replaced all-set)))

