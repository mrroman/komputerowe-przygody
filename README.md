# Breakout

Game written during Clojure - "Komputerowe przygody" (computer adventures) lecture.

# Starting game

1. Start REPL.
```sh
lein repl
```
2. Require breakout.core namespace.
```clojure
(require 'breakout.core)
```

# License

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.